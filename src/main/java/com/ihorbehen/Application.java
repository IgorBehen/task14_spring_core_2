package com.ihorbehen;

import com.ihorbehen.config_data.ConfigProfOne;
import com.ihorbehen.config_data.ConfigProfTwo;
import com.ihorbehen.pojo_beans1.bean_collection.GeneralSongBean;
import com.ihorbehen.pojo_beans1.bean_collection.JazzSong;
import com.ihorbehen.pojo_beans1.bean_collection.Song;
import com.ihorbehen.pojo_beans1.beans1.BeanA;
import com.ihorbehen.pojo_beans1.beans1.BeanB;
import com.ihorbehen.pojo_beans1.beans1.BeanC;
import com.ihorbehen.pojo_beans1.beans2.NarcissusFlower;
import com.ihorbehen.pojo_beans1.beans2.RoseFlower;
import com.ihorbehen.pojo_beans1.beans3.BeanD;
import com.ihorbehen.pojo_beans1.beans3.BeanF;
import com.ihorbehen.pojo_beans1.beansOther.*;
import com.ihorbehen.pojo_beans1.profile_data.Data;
import com.ihorbehen.pojo_beans1.profile_data_component.MyData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import com.ihorbehen.config_data_component.ConfigCopm;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigOne.class, ConfigTwo.class, ConfigThree.class);
        for (String beanName : context.getBeanDefinitionNames())
            System.out.println(context.getBean(beanName).getClass().toString());

        BeanA beanA = context.getBean(BeanA.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        BeanD beanD = context.getBean(BeanD.class);
        //BeanE beanE = context.getBean(BeanE.class); - �� �������� � @ComponentScan
        BeanF beanF = context.getBean(BeanF.class);
        //CatAnimal catAnimal = context.getBean(CatAnimal.class); - �� �������� � @ComponentScan
        NarcissusFlower narcissusFlower = context.getBean(NarcissusFlower.class);
        RoseFlower roseFlower = context.getBean(RoseFlower.class);
        OtherBeanA otherBeanA = context.getBean(OtherBeanA.class);
        OtherBeanB otherBeanB = context.getBean(OtherBeanB.class);
        OtherBeanC otherBeanC = context.getBean(OtherBeanC.class);
        OtherBeanD otherBeanD = context.getBean(OtherBeanD.class);
        OtherBean otherBean = context.getBean(OtherBean.class);
        GeneralSongBean generalSongBean = context.getBean(GeneralSongBean.class).printSongs();

        System.out.println(beanA);
        System.out.println(beanB);
        System.out.println(beanC);
        System.out.println(beanD);
        //System.out.println(beanE); - �� �������, �� �������� � @ComponentScan
        System.out.println(beanF);
        //System.out.println(catAnimal); - �� �������, �� �������� � @ComponentScan
        System.out.println(narcissusFlower);
        System.out.println(roseFlower);
        System.out.println(otherBeanA);
        System.out.println(otherBeanB);
        System.out.println(otherBeanC);
        System.out.println(otherBeanD);
        System.out.println(otherBean);
        System.out.println(generalSongBean);
        System.out.println(context.getBean(Song.class));
        System.out.println(context.getBean(JazzSong.class));
        System.out.println(context.getBean("popSong"));
        System.out.println(context.getBean("generalSongBean"));

        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(System.identityHashCode(otherBeanA));
        System.out.println(System.identityHashCode(otherBeanA));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(System.identityHashCode(otherBeanB));
        System.out.println(System.identityHashCode(otherBeanB));
        System.out.println(context.getBean("getOtherBeanC"));
        System.out.println(context.getBean("getOtherBeanC"));
        System.out.println(System.identityHashCode(otherBeanC));
        System.out.println(System.identityHashCode(otherBeanC));

        //@Profiles by @Bean
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "one");
        AnnotationConfigApplicationContext context1 =
                new AnnotationConfigApplicationContext(ConfigProfOne.class);
        System.out.println(context1.getBean(Data.class));

        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "two");
        AnnotationConfigApplicationContext context2 =
                new AnnotationConfigApplicationContext(ConfigProfTwo.class);
        System.out.println(context2.getBean(Data.class));

        //@Profiles by @Component
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "oneComp");
        AnnotationConfigApplicationContext context3 =
                new AnnotationConfigApplicationContext(ConfigCopm.class);
        System.out.println(context3.getBean(MyData.class));

        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "twoComp");
        AnnotationConfigApplicationContext context4 =
                new AnnotationConfigApplicationContext(ConfigCopm.class);
        System.out.println(context4.getBean(MyData.class));
    }
}

