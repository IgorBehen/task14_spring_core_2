package com.ihorbehen;

import com.ihorbehen.pojo_beans1.beansOther.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.ihorbehen.pojo_beans1.beans1")
@ComponentScan(basePackages = "com.ihorbehen.pojo_beans1.beansOther", useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = OtherBean.class))
@PropertySource("my.properties")
public class ConfigOne {

    @Value("${OtherBeanA.b}")
    private String b;
    @Value("${OtherBeanA.a}")
    private int a;
    @Bean
    public OtherBeanA getOtherBeanA() {
        return new OtherBeanA(a, b);
    }

    @Bean
    public OtherBeanB getOtherBeanB(OtherBeanA OtherBeanA) {
        return new OtherBeanB (OtherBeanA);
    }

    @Bean
    @Autowired
    @Scope("prototype")
    public OtherBeanC getOtherBeanC(OtherBeanB otherBeanB) {
        OtherBeanC otherBeanC = new OtherBeanC();
        otherBeanC.setB("This is a OtherBeanC");
        otherBeanC.setA(2);
        return otherBeanC;
    }

    @Bean
    public OtherBeanD getOtherBeanD() {
        return new OtherBeanD();
    }
}

