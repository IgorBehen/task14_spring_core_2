package com.ihorbehen;

import com.ihorbehen.pojo_beans1.bean_collection.GeneralSongBean;
import com.ihorbehen.pojo_beans1.bean_collection.JazzSong;
import com.ihorbehen.pojo_beans1.bean_collection.PopSong;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ihorbehen.pojo_beans1.bean_collection")
public class ConfigThree {

    @Bean("jazzSong")
    public JazzSong getJazzSong(){
        return new JazzSong("jazzSong");
    }

    @Bean("popSong")
     public PopSong getPopSong(){
            return new PopSong("popSong");
    }

    @Bean("generalSongBean")
    public GeneralSongBean getGeneralSongBean(){
        return new GeneralSongBean("generalSongBean");
    }
}
