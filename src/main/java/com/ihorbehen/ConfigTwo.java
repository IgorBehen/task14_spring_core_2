package com.ihorbehen;

import com.ihorbehen.pojo_beans1.beans3.BeanD;
import com.ihorbehen.pojo_beans1.beans3.BeanE;
import com.ihorbehen.pojo_beans1.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.ihorbehen.pojo_beans1.beans2", useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.REGEX,
                pattern = "com.ihorbehen.pojo_beans1.beans2..+Flower"))
@ComponentScan(basePackages = "com.ihorbehen.pojo_beans1.beans3", useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {BeanD.class, BeanF.class}),
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = BeanE.class))
public class ConfigTwo {


}
