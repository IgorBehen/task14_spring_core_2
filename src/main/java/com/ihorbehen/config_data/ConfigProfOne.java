package com.ihorbehen.config_data;

import com.ihorbehen.pojo_beans1.profile_data.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
@Profile("one")
public class ConfigProfOne {
    @Bean
    public Data getData() {
        System.out.print("ConfigProfOne: ");
        String name = "Andrew";
        int age = 23;
        return new Data(name, age);
    }
}

