package com.ihorbehen.config_data;

import com.ihorbehen.pojo_beans1.profile_data.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
@Profile("two")
public class ConfigProfTwo {
    @Bean
    public Data getData() {
        System.out.print("ConfigProfTwo: ");
        String name = "Anastasia";
        int age = 21;
        return new Data(name, age);
    }
}
