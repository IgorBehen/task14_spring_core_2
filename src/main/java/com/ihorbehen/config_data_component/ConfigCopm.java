package com.ihorbehen.config_data_component;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ihorbehen.pojo_beans1.profile_data_component")
public class ConfigCopm {
}
