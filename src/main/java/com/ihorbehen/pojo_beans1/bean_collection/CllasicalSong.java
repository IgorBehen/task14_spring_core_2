package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class CllasicalSong implements Song{
    @Override
    public String getSong() {
        return "Ludwig van Beethoven � 'Symphony No. 5'";
    }
}