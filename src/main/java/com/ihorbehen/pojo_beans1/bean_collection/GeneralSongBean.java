package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralSongBean {
    @Autowired
    private List<Song> songs;
    String generalSongBean;

    public GeneralSongBean(String generalSongBean) {
        this.generalSongBean = generalSongBean;
    }

    public GeneralSongBean printSongs() {
        for (Song song : songs) {
            System.out.println(song.getSong());
        }
        return null;
    }
}
