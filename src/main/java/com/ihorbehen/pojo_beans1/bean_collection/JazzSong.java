package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class JazzSong implements Song {
    private String jazzSong;
    public JazzSong(String jazzSong) {
        this.jazzSong = jazzSong;
        }

    @Override
    public String getSong() {
        return "Miles Davis - 'So What'";
    }
}
