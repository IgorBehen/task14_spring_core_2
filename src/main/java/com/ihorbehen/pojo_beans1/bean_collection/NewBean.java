package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class NewBean {
    @Autowired
    Song bean1;

    @Autowired
    @Qualifier("cllasicalSong")
    Song bean2;

    @Override
    public String toString() {
        return "NewBean{" +
                "bean1=" + bean1 +
                ", bean2=" + bean2 +
                '}';
    }
}
