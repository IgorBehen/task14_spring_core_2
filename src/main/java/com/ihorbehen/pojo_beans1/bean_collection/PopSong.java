package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public class PopSong implements Song {
    private String popSong;
    public PopSong(String popSong) {
        this.popSong = popSong;
    }

    @Override
    public String getSong() {
        return "Madonna - 'Holiday'";
    }
}

