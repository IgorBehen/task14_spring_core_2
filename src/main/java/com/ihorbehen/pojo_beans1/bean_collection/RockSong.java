package com.ihorbehen.pojo_beans1.bean_collection;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
public class RockSong implements Song{

    @Override
    public String getSong() {
        return "LED ZEPPELIN - 'STAIRWAY TO HEAVEN'";
    }
}
