package com.ihorbehen.pojo_beans1.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private int a;
    private String b;

    public RoseFlower() {
    }

    public RoseFlower(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
