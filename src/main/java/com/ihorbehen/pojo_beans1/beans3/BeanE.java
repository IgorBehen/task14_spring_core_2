package com.ihorbehen.pojo_beans1.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {
    private int a;
    private String b;

    public BeanE() {
    }

    public BeanE(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
