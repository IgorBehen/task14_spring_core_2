package com.ihorbehen.pojo_beans1.beansOther;

import org.springframework.stereotype.Component;

@Component
public class OtherBean {
    private int a = 1;
    private String b = "asd";

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "OtherBean{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}