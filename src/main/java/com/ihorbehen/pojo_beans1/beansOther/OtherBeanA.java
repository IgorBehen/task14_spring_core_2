package com.ihorbehen.pojo_beans1.beansOther;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanA {
    private int a;
    private String b;
    private OtherBean otherBean;

    @Autowired
    public void setOtherBean(OtherBean otherBean) {
        this.otherBean = otherBean;
        System.out.println("OtherBeanA --> setOtherBean(): " + otherBean + " => by Setter");
    }

    public OtherBeanA() {
    }

    public OtherBeanA(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "a=" + a +
                ", b='" + b + '\'' +
                ", otherBean=" + otherBean +
                '}';
    }
}
