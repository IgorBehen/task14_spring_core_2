package com.ihorbehen.pojo_beans1.beansOther;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanB {
    private int a;
    private String b;
    private OtherBeanA otherBeanA;

    public OtherBeanB(int a, String b) {
        this.a = a;
        this.b = b;
    }

    @Autowired
    public OtherBeanB(OtherBeanA otherBeanA) {
        System.out.println("OtherBeanB --> setOtherBean(): " + otherBeanA + " => by Constructor");
        this.otherBeanA = otherBeanA;
    }

    public int getA() {
        return a;
    }


    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "a=" + a +
                ", b='" + b + '\'' +
                ", otherBeanA=" + otherBeanA +
                '}';
    }
}
