package com.ihorbehen.pojo_beans1.beansOther;

import com.ihorbehen.pojo_beans1.beans1.BeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("otherBeanCQualifier")
public class OtherBeanC {
    private int a;
    private String b;

  @Autowired
  OtherBeanB otherBeanB;


    public OtherBeanC() {
    }

    public OtherBeanC(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "otherBeanB=" + otherBeanB +
                ", a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
