package com.ihorbehen.pojo_beans1.beansOther;

import org.springframework.beans.factory.annotation.Qualifier;

public class OtherBeanD {
    private int a;
    private String b;

    @Qualifier("otherBeanCQualifier")
    private OtherBeanC otherBeanC;

    public OtherBeanD() {
    }

    public OtherBeanD(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "OtherBeanD{" +
                "OtherBeanC =" + otherBeanC +
                ", a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
