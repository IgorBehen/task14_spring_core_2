package com.ihorbehen.pojo_beans1.profile_data;

public class Data {
        private String name;
        private int age;

    public Data(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Data{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
