package com.ihorbehen.pojo_beans1.profile_data_component;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("oneComp")
public class DataCompOne extends DataComp {

    public DataCompOne() {
        name = "Oleg";
        age = 18;
    }

    @Override
    public String toString() {
        return "DataCompOne: {" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

