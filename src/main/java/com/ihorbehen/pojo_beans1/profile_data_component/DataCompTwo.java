package com.ihorbehen.pojo_beans1.profile_data_component;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("twoComp")
public class DataCompTwo extends DataComp {

    public DataCompTwo() {
        name = "Alise";
        age = 22;
    }

    @Override
    public String toString() {
        return "DataCompTwo: {" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
