package com.ihorbehen.pojo_beans1.profile_data_component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyData {
    @Autowired
    private DataComp dataComp;

    public String toString() {
        return dataComp.toString();
    }
}


